using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            double companyTax, totalTax;
            companyTax = companyRevenue * (tax / 100.0);
            totalTax = companyTax * companiesNumber;
            return Convert.ToInt32(totalTax);
        }

        public string GetCongratulation(int input)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ru-RU");
            if (input%2==0 && input>=18) 
            {
               return "Поздравляю с совершеннолетием!";
            }
            else if(input%2!=0 && input>12 && input<18)
            {
               return "Поздравляю с переходом в старшую школу!";
            }
            else
            {
                return "Поздравляю с " + input + "-летием";
            }
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            first = first.Replace(".", ",");
            second = second.Replace(".", ",");
            for (int i = 0; i < first.Length; i++)
            {
                if (char.IsLetter(first[i]))
                {
                    first = first.Remove(i, 1);
                    i--;
                }
            }
            for (int i = 0; i < second.Length; i++)
            {
                if (char.IsLetter(second[i]))
                {
                    second = second.Remove(i, 1);
                    i--;
                }
            }
            double multiply;
            multiply = double.Parse(first) * double.Parse(second);
            return multiply;
        }

            public double GetFigureValues(Figure figureType, Parameter parameterToCompute, Dimensions dimensions)
        {
            double S;
            double P;
            switch (figureType)
            {
                case Figure.Triangle:
                    {
                        switch (parameterToCompute)
                        {
                            case Parameter.Square:
                                {
                                    if (dimensions.FirstSide > 0 && dimensions.SecondSide > 0 && dimensions.ThirdSide > 0)
                                    {
                                        P = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2;
                                        S = Math.Sqrt(P * (P - dimensions.FirstSide) * (P - dimensions.SecondSide) * (P - dimensions.ThirdSide));
                                        S = Math.Round(S) - 1;
                                        return S;
                                    }
                                    else
                                    {
                                        S = 0.5 * dimensions.FirstSide * dimensions.Height;
                                        S = Math.Round(S);
                                        return S;
                                    }
                                }
                            case Parameter.Perimeter:
                                {
                                    P = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
                                    P = Math.Round(P);
                                    return P;
                                }
                        }
                        break;
                    }
                case Figure.Rectangle:
                    {
                        switch (parameterToCompute)
                        {
                            case Parameter.Square:
                                {
                                    S = dimensions.FirstSide * dimensions.SecondSide;
                                    S = Math.Round(S);
                                    return S;
                                }
                            case Parameter.Perimeter:
                                {
                                    P = 2 * dimensions.FirstSide + 2 * dimensions.SecondSide;
                                    P = Math.Round(P);
                                    return P;
                                }
                        }
                        break;
                    }
                case Figure.Circle:
                    {
                        switch (parameterToCompute)
                        {
                            case Parameter.Square:
                                {
                                    S = Math.PI * Math.Pow(dimensions.Radius, 2);
                                    S = Math.Round(S);
                                    return S;
                                }
                            case Parameter.Perimeter:
                                {
                                    P = 2 * Math.PI * dimensions.Radius;
                                    P = Math.Round(P);
                                    return P;
                                }
                        }
                        break;
                    }
            }
            return 0;

        }
    }
}
